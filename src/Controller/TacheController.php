<?php

namespace App\Controller;

use App\Entity\Tache;
use App\Form\TacheType;
use App\Repository\TacheRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class TacheController extends AbstractController
{

    private $tacheRepository;
    private $em;

    public function __construct(TacheRepository $tacheRepository,
                                EntityManagerInterface $em)
    {
        $this->tacheRepository = $tacheRepository;
        $this->em = $em;
    }

    /**
     * @Route("/taches", name="taches")
     */
    public function index()
    {
        $taches = $this->tacheRepository->findAll();
        return $this->render('tache/index.html.twig'
            , ['taches' => $taches]);
    }

    /**
     * @Route("/tache/show/{id}", name="tache_show", methods={"GET"})
     */
    public function show($id)
    {
        $tache = $this->tacheRepository->find($id);
        return $this->render('tache/show.html.twig'
            , ['tache' => $tache]);
    }

    /**
     * @Route("/tache/edit/{id}", name="edit_tache", methods={"POST","GET"})
     */
    public function edit(Request $request, $id)
    {
        $tache = $this->tacheRepository->find($id);
        $form = $this->createForm(TacheType::class, $tache);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($tache);
            $this->em->flush();
            return $this->redirectToRoute('taches');
        }
        return $this->render('tache/edit.html.twig'
            , ['form' => $form->createView()]);
    }

    /**
     * @Route("/tache/delete/{id}", name="delete_tache", methods={"DELETE","GET"})
     */
    public function delete($id)
    {
        $tache = $this->tacheRepository->find($id);
        $this->em->remove($tache);
        $this->em->flush();
        return $this->redirectToRoute('taches');
    }

    /**
     * @Route("/tache/new", name="newtache", methods={"POST","GET"})
     */
    public function new(Request $request)
    {
        $tache= new Tache();
        $form = $this->createForm(TacheType::class,$tache);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $tache = $form->getData();
            $this->em->persist($tache);
            $this->em->flush();
            return $this->redirectToRoute('taches');
        }
        return $this->render('tache/new.html.twig'
            , ['form' => $form->createView()]);

    }
}
