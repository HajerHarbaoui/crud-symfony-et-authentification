<?php

namespace App\Controller;

use App\Entity\Projet;
use App\Form\ProjetType;
use App\Repository\ProjetRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProjetController extends AbstractController
{
    private $projetRepository;
    private $em;

    public function __construct(ProjetRepository $projetRepository,
                                EntityManagerInterface $em)
    {
        $this->projetRepository = $projetRepository;
        $this->em = $em;
    }

    /**
     * @Route("/projets", name="projets")
     */
    public function index()
    {
        $projets = $this->projetRepository->findAll();
        return $this->render('projet/index.html.twig'
            , ['projets' => $projets]);
    }

    /**
     * @Route("/projet/show/{id}", name="projet_show", methods={"GET"})
     */
    public function show($id)
    {
        $projet = $this->projetRepository->find($id);
//        dd($this->projetRepository->selectTache());
        $taches=$this->projetRepository->selectTache($id);
        return $this->render('projet/show.html.twig'
            , ['projet' => $projet, 'taches'=>$taches]);
    }

    /**
     * @Route("/projet/delete/{id}", name="delete_projet", methods={"DELETE","GET"})
     */
    public function delete($id)
    {
        $projet = $this->projetRepository->find($id);
        $this->em->remove($projet);
        $this->em->flush();
        return $this->redirectToRoute('projets');
    }

    /**
     * @Route("/projet/edit/{id}", name="edit_projet", methods={"POST","GET"})
     */
    public function edit(Request $request, $id)
    {
        $projet = $this->projetRepository->find($id);
        $form = $this->createForm(ProjetType::class, $projet);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($projet);
            $this->em->flush();
            return $this->redirectToRoute('projets');
        }
        return $this->render('projet/edit.html.twig'
            , ['form' => $form->createView()]);
    }

    /**
     * @Route("/projet/new", name="newprojet", methods={"POST","GET"})
     */
    public function new(Request $request)
    {
        $projet= new Projet();
        $form = $this->createForm(ProjetType::class,$projet);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $projet = $form->getData();
            $this->em->persist($projet);
            $this->em->flush();
            return $this->redirectToRoute('projets');
        }
        return $this->render('projet/new.html.twig'
            , ['form' => $form->createView()]);

    }
}
